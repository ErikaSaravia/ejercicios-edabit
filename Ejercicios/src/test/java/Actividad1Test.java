/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author pima_
 */
public class Actividad1Test {
    
    //Basic Calculator
    
  @Test
  public void test1() {
    assertEquals(1, Actividad1.calculator(2, '/', 2));
  }
	
	@Test
  public void test2() {
    assertEquals(3, Actividad1.calculator(10, '-', 7));
  }
	
	@Test
  public void test3() {
    assertEquals(32, Actividad1.calculator(2, '*', 16));
  }
	
	
	@Test
  public void test4() {
    assertEquals(41, Actividad1.calculator(15, '+', 26));
  }
  
  //Triangular Number Sequence
   @Test
  public void testT1() {
    assertEquals(1, Actividad1.triangle(1));
  }
	
	@Test
  public void testt2() {
    assertEquals(3, Actividad1.triangle(2));
  }
	
	@Test
  public void testT3() {
    assertEquals(6, Actividad1.triangle(3));
  }
  
  //Convert Age to Days
  
  @Test
	public void test01() {
		assertEquals(Actividad1.calcAge(10), 3650);
	}
	
	@Test
	public void test02() {
		assertEquals(Actividad1.calcAge(0), 0);
	}
	
	@Test
	public void test03() {
		assertEquals(Actividad1.calcAge(73), 26645);
	}
}
    

